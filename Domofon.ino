#include <SoftwareSerial.h>
int input; 
String s;

const int l0 = 12;
const int l1 = 11;
const int l2 = 10;
const int l3 = 9;
const int l4 = 8;
const int l5 = 7;
const int l6 = 6;
const int l7 = 5;
const int l8 = 4;
const int l9 = 3;

const int buz = 13;
const int led = 22;

int b1State = 0;
int b2State = 0;
int b3State = 0;
int b4State = 0;
int b5State = 0;
int b6State = 0;
int b7State = 0;
int b8State = 0;
int b9State = 0;
int b10State = 0;

SoftwareSerial RFIDSerial(50, 51); // RX, TX
void setup() 
{ 
  pinMode(l0, OUTPUT);
  pinMode(l1, OUTPUT);
  pinMode(l2, OUTPUT);
  pinMode(l3, OUTPUT);
  pinMode(l4, OUTPUT);
  pinMode(l5, OUTPUT);
  pinMode(l6, OUTPUT);
  pinMode(l7, OUTPUT);
  pinMode(l8, OUTPUT);
  pinMode(l9, OUTPUT);

  pinMode(led, OUTPUT);
  // Очищаем буфер  
  Serial.flush(); 
  // Устанавливаем скорость работы с портом 
  Serial.begin(9600); 
  // Устанавливаем скорость работы с модулем RDM630 
  RFIDSerial.begin(9600); 
  s=""; 

}
void loop() 
{ 
  digitalWrite(led, HIGH);
  
  if (b1State == 1) {
    digitalWrite(l0, HIGH);
  }
  if (b2State == 1) {
    digitalWrite(l1, HIGH);
  }
  if (b3State == 1) {
    digitalWrite(l2, HIGH);
  }
  if (b4State == 1) {
    digitalWrite(l3, HIGH);
  }
  if (b5State == 1) {
    digitalWrite(l4, HIGH);
  }
  if (b6State == 1) {
    digitalWrite(l5, HIGH);
  }
  if (b7State == 1) {
    digitalWrite(l6, HIGH);
  }
  if (b8State == 1) {
    digitalWrite(l7, HIGH);
  }
  if (b9State == 1) {
    digitalWrite(l8, HIGH);
  }
  if (b10State == 1) {
    digitalWrite(l9, HIGH);
  }
//  delay(500);
//  digitalWrite(l0, LOW);
//  digitalWrite(l1, LOW);
//  digitalWrite(l2, LOW);
//  digitalWrite(l3, LOW);
//  digitalWrite(l4, LOW);
//  digitalWrite(l5, LOW);
//  digitalWrite(l6, LOW);
//  digitalWrite(l7, LOW);
//  digitalWrite(l8, LOW);
//  digitalWrite(l9, LOW);
  
  // если есть данные, то 
  if (RFIDSerial.available() > 0) { 
    // читаем блок данных с модуля RDM630 и заносим их в переменную input 
    input = RFIDSerial.read(); 
    // присваиваем все считанные в переменную input значения переменной s, так как за 1 цикл loop мы получаем 1 значение, а их 12   
    s+=input;  // то же самое, что и s=s+input; 
    // если длинна кода равна 26 символам (в памяти чипа можно разместить 26 байт информации), то 
    if (s.length()==26) { 
      // выводим данные 
      switch (s.toInt()) {
        case 24950484853685166506653703:
          Serial.println("lignt green"); 
          b4State = 1; 
          tone(buz, 3000, 300);
          break;
        case 24852484849575070575165493:
          Serial.println("black green"); 
          b10State = 1; 
          tone(buz, 3000, 300);
          break;
        case 24950484850685151705370573:
          Serial.println("blue"); 
          b5State = 1; 
          tone(buz, 3000, 300);
          break;
        case 24949484855665167525549493:
          Serial.println("orange"); 
          b9State = 1; 
          tone(buz, 3000, 300);
          break;
        case 24950484853677069566851683:
          Serial.println("black"); 
          b6State = 1; 
          tone(buz, 3000, 300);
          break;
        case 24868484852506970495666563:
          Serial.println("yellow"); 
          b8State = 1; 
          tone(buz, 3000, 300);
          break;
        case 24950484853684852674856663:
          Serial.println("grey"); 
          b3State = 1; 
          tone(buz, 3000, 300);
          break;
        case 24950484850575270566870573:
          Serial.println("red"); 
          b7State = 1; 
          tone(buz, 3000, 300);
          break;
        case 24948484857495748504951483:
          Serial.println("black 1");
          b1State = 1; 
          tone(buz, 3000, 300);
          break;
        case 24950484853656651677051523:
          Serial.println("black 2"); 
          b2State = 1; 
          tone(buz, 3000, 300);
          break;
        
      }
      //Serial.println(s); 
      // очищаем переменную 
      s=""; 
      //digitalWrite(buz, LOW);
    } 
  } 
  //delay(500);
}
